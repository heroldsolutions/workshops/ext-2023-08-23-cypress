- [1. Enable Customer API](#1-enable-customer-api)
- [2. Intercepting with fixtures](#2-intercepting-with-fixtures)
- [3. Customer Count](#3-customer-count)
- [4. API Test](#4-api-test)
- [5. Connection to Chat should show Chat menu item](#5-connection-to-chat-should-show-chat-menu-item)
- [6. Failed WebSocket connection](#6-failed-websocket-connection)
- [7. Sending messages](#7-sending-messages)
- [8. Verify the messages in the chat window are shown](#8-verify-the-messages-in-the-chat-window-are-shown)
- [9. WebSocket Proxy Pattern](#9-websocket-proxy-pattern)

This branch includes a chat feature that uses a WebSocket connection.

You have to install and start the chat app first.

- Go to **./ws-app**, run `npm ci`.
- From the project's root directory, run `npm run websocket:start`
- In the console, you should see "WebSocket up and running..."

# 1. Enable Customer API

At the moment, all requests in the customers module are mocked by the application itself.

Append a test in **customers.cy.ts**, where you disable the application mocking. This can be done by clicking on the toggler in home.

Verify that a real request is sent out, when you click on the customers menu item. In order to get to the URL, you can watch the Cypress log in the test runner.

<details>
<summary>Show Solution</summary>
<p>

**/cypress/e2e/customers.cy.ts**

```typescript
it("should not mock the network in customers", () => {
  cy.testid("tgl-mock-customers").click();
  cy.intercept("GET", "https://api.eternal-holidays.net/customers?page=0&pageSize=10").as("customersRequest");
  cy.findByRole("link", { name: "Customers" }).click();
  cy.wait("@customersRequest").its("response.statusCode").should("eq", 200);
});
```

  </p>
  </details>

# 2. Intercepting with fixtures

Split up your tests into two parts. One, where the network is mocked by the application and another, where Cypress uses `cy.intercept`. You can do that by adding two nested `describe` methods.

Next, add a fixture called **./fixtures/customers.json** that contains the response for the customers endpoint. Add a single customer with some dummy data.

In the "not-mocked" section in **customers.cy.ts**, make use of that fixture.

Verify that the "fixture-based" customer is shown.

<details>
<summary>Show Solution</summary>
<p>

**/cypress/fixtures/customers.json**

```json
{
  "total": 1,
  "content": [
    {
      "id": 1,
      "firstname": "Konrad",
      "name": "Niedermeyer"
    }
  ]
}
```

**/cypress/e2e/customers.cy.ts**

```typescript
it("should use a fixture to mock the customers", () => {
  cy.visit("?mock-customers=0");

  cy.intercept("GET", "https://api.eternal-holidays.net/customers?page=0&pageSize=10", {
    fixture: "customers.json",
  });
  cy.findByRole("link", { name: "Customers" }).click();
  cy.findByLabelText("Konrad Niedermeyer").should("be.visible");
});
```

  </p>
  </details>

# 3. Customer Count

Intercept the request for customers without mocking it. Count the numbers of customers retrieved and use that number for the assertion.

<details>
<summary>Show Solution</summary>
<p>

**/cypress/e2e/customers.cy.ts**

```typescript
it("should verify that the right amount of customers is shown", () => {
  let customersCount: number | undefined = undefined;
  cy.visit("?mock-customers=0");
  cy.intercept("https://api.eternal-holidays.net/customers**", (req) => {
    req.continue((res) => (customersCount = res.body.content.length));
  }).as("customersRequest");

  cy.findByRole("link", { name: "Customers" }).click();
  cy.wait("@customersRequest");
  cy.testid("row-customer").should((customerRows) => {
    expect(customerRows.length).to.eq(customersCount);
  });
});
```

  </p>
  </details>

# 4. API Test

Write an API test, where you create a new customer, edit it and remove it. Between the three steps, check if the API returns the right customer data:

- List existing customers: GET - https://api.eternal-holidays.net/customers
- Add a new customer: POST - https://api.eternal-holidays.net/customers
- Edit an existing customer: PUT - https://api.eternal-holidays.net/customers
- Delete customer: DELETE - https://api.eternal-holidays.net/customers/{id}

An example data for a customer is:

```typescript
const anika: Partial<Customer> = {
  firstname: "Anika",
  name: "Luhmann",
  country: "DE",
  birthdate: "1999-03-25T23:00:00.000Z",
};
```

**Hint 1**: The POST's body must not include an `id` property.

**Hint 2**: You can nest further request in the `then` method of a `cy.request`. You will need nested requests, because you have to store and re-use the id of the generated user.

<details>
<summary>Show Solution</summary>
<p>

**cypress/e2e/customers.cy.ts**

```typescript
it("should test the API", () => {
  let id = 0;
  const anika: Partial<Customer> = {
    firstname: "Anika",
    name: `Luhmann-${new Date().toISOString()}`,
    country: "DE",
    birthdate: "1999-03-25T23:00:00.000Z",
  };
  cy.request("POST", "https://api.eternal-holidays.net/customers", anika).then(({ body }) => {
    id = body.id;

    cy.request("https://api.eternal-holidays.net/customers").should((response) => {
      const customers: Customer[] = response.body.content;
      const customer = customers.find((customer) => customer.id === id);
      expect(customer?.firstname).to.eq(anika.firstname);
      expect(customer?.name).to.eq(anika.name);
    });

    cy.request("PUT", "https://api.eternal-holidays.net/customers", {
      ...anika,
      id,
      country: "UK",
    });

    cy.request("https://api.eternal-holidays.net/customers").should((response) => {
      const customers: Customer[] = response.body.content;
      const customer = customers.find((customer) => customer.id === id);
      expect(customer?.country).to.eq("UK");
    });

    cy.request("DELETE", `https://api.eternal-holidays.net/customers/${id}`);
    cy.request("https://api.eternal-holidays.net/customers").should((response) => {
      const customers: Customer[] = response.body.content;
      const customer = customers.find((customer) => customer.id === id);
      expect(customer).to.be.undefined;
    });
  });
});
```

  </p>
  </details>

# 5. Connection to Chat should show Chat menu item

Write a test, where you override the exposed `window.wsClient`. It should be a function that expects a callback. Execute that callback and pass on `{status: 'connected'}` as parameter. This is the signal for the application, that the connection to the WebSocket was successful.

Next to that, verify that a chat link pops up in the sidemenu.

<details>
<summary>Show Solution</summary>
<p>

**cypress/e2e/chat.cy.ts**

```typescript
type WsData = {
  message?: string;
  status?: string;
};

type WsClient = {
  (callback: (value: WsData) => void): void;
};

declare namespace Cypress {
  interface Chainable {
    window(): Chainable<{ wsClient: WsClient }>;
  }
}

describe("Chat", () => {
  it("should connect to the WebSocket", () => {
    cy.visit("");
    cy.window().then((w) => {
      w.wsClient = (callback: (value: WsData) => void) => {
        callback({ status: "connected" });
      };
    });
    cy.findByRole("button", { name: "Enable Chat" }).click();
    cy.findByText("Connection established").should("be.visible");
    cy.findByRole("link", { name: "Chat" }).should("be.visible");
  });
});
```

</p>
</details>

# 6. Failed WebSocket connection

Verify that the application shows an error message "Connection could not be established", when the function mocked function throws an errors.

<details>
<summary>Show Solution</summary>
<p>

**/cypress/e2e/chat.cy.ts**

```typescript
it("should fail in connecting to the WebSocket", () => {
  cy.visit("");
  cy.window().then((w) => {
    w.wsClient = (callback: (value: WsData) => void) => {
      throw new Error("nothing works here");
    };
  });
  cy.findByRole("button", { name: "Enable Chat" }).click();
  cy.findByText("Could not establish connection").should("be.visible");
});
```

</p>
</details>

# 7. Sending messages

Send two messages through the WebSocket and verify that the chat menu item shows a badge of 2. The message should be sent in an interval of half a second.

<details>
<summary>Show Solution</summary>
<p>

**/cypress/e2e/chat.cy.ts**

```typescript
it("should show the total amount of chat messages in the menu link", () => {
  cy.visit("");
  cy.window().then((w) => {
    w["wsClient"] = (callback: (value: WsData) => void) => {
      callback({ status: "connected" });
      callback({ message: "This is the first message" });
      callback({ message: "This is the second message" });
    };
  });

  cy.findByRole("button", { name: "Enable Chat" }).click();
  cy.findByRole("link", { name: "Chat" }).find(".mat-badge-content").should("have.text", 2);
});
```

  </p>
  </details>

# 8. Verify the messages in the chat window are shown

Extend the former test, click on the chats and verify that the two messages are shown.

<details>
<summary>Show Solution</summary>
<p>

**/cypress/e2e/chat.cy.ts**

```typescript
it("should show the total amount of chat messages in the menu link", () => {
  cy.visit("");
  cy.window().then((w) => {
    w.wsClient = (callback: (value: WsData) => void) => {
      callback({ status: "connected" });
      callback({ message: "This is the first message" });
      callback({ message: "This is the second message" });
    };
  });

  cy.findByRole("button", { name: "Enable Chat" }).click();
  cy.findByRole("link", { name: "Chat" }).as("chatLink");
  cy.get("@chatLink").find(".mat-badge-content").should("have.text", 2);
  cy.get("@chatLink").click();
  cy.findByText("This is the first message").should("be.visible");
  cy.findByText("This is the second message").should("be.visible");
});
```

  </p>
  </details>

# 9. WebSocket Proxy Pattern

Apply the proxy pattern, where Cypress itself opens a connection to the WebSocket and verifies that it sends a `{status: 'connected'}` upcon click on the Cient button.

There is no solution for that exercise.
