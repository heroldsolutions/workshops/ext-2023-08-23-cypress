- [1. Warm-Up](#1-warm-up)
- [2. Request Firenze in "testing-library style"](#2-request-firenze-in-testing-library-style)
- [3. Add a new User in "testing-library style"](#3-add-a-new-user-in-testing-library-style)
- [4. Axe check against landing page](#4-axe-check-against-landing-page)
- [5. Failing Test I: _should rename Angelika Hoffmann to Angelika Hofmann_](#5-failing-test-i-should-rename-angelika-hoffmann-to-angelika-hofmann)
- [6. Buggy Test II: _should remove Knut Eggen_](#6-buggy-test-ii-should-remove-knut-eggen)
- [7. Buggy Test III: _should toggle the loading indicator_](#7-buggy-test-iii-should-toggle-the-loading-indicator)
- [8. Scraping with `cy.task`](#8-scraping-with-cytask)

Checkout the branch _cy-02-advanced-starter_. Since it contains changes in the application, you have to switch the branch.

The solution branch is _cy-02-advanced-solution_.

# 1. Warm-Up

Cypress has pre-defined resolutions which can be used by `cy.viewport("preset")`. Run the _should count the entities_ in **customers.cy.ts** test with following presets

- ipad-2
- ipad-mini
- iphone-6
- samsung-s10

<details>
<summary>Show Solution</summary>
<p>

**/cypress/e2e/customers.cy.ts**

```typescript
(
  ["ipad-2", "ipad-mini", "iphone-6", "samsung-s10"] as ViewportPreset[]
).forEach((preset) => {
  it(`should count the entries in ${preset}`, () => {
    cy.viewport(preset);
    cy.visit("");
    cy.testid("btn-customers").click();
    cy.testid("row-customer").should("have.length", 10);
  });
});
```

</p>
</details>

# 2. Request Firenze in "testing-library style"

Rewrite the test **should request brochure for Firenze** in **holidays.cy.ts** where you only use the find queries from the testing library. So no `cy.get` or `cy.contains`, but `cy.findByRole` and `cy.findByLabelText`.

<details>
<summary>Show Solution</summary>
<p>

**/cypress/e2e/holidays.cy.ts**

```typescript
it("should request brochure for Firenze", () => {
  cy.findByRole("link", { name: "Holidays" }).click();
  cy.findByLabelText(/Firenze/)
    .findByRole("link", { name: "Get a Brochure" })
    .click();
  cy.findByLabelText("Address").type("Domgasse 5");
  cy.findByRole("button", { name: "Send" }).click();
  cy.findByRole("status").should("have.text", "Brochure sent");
});
```

</p>
</details>

# 3. Add a new User in "testing-library style"

As you've done in the holidays, rewrite **should add a new customer** in **customers.cy.ts** and use the testing library.

<details>
<summary>Show Solution</summary>
<p>

**/cypress/e2e/customers.cy.ts**

```typescript
it("should add a new customer", () => {
  cy.findByRole("link", { name: "Customers" }).click();
  cy.findByRole("link", { name: "Add Customer" }).click();
  cy.findByLabelText("Firstname").type("Tom");
  cy.findByLabelText("Name").type("Lincoln");
  cy.findByLabelText("Country").click();
  cy.findByRole("option", { name: "USA" }).click();
  cy.findByLabelText("Birthdate").type("12.10.1995");
  cy.findByRole("button", { name: "Save" }).click();
  cy.findByRole("button", { name: "next" }).click();

  cy.findByLabelText("Tom Lincoln").should("be.visible");
});
```

</p>
</details>

# 4. Axe check against landing page

It should verify if our landing page is accessible.

- Verify that the packages `cypress-axe` and `axe-core` are installed.
- Activate the a11y plugin by adding the following line in **cypress//support/e2e.ts**: `import 'cypress-axe';`
- Create a new file **cypress/e2e/a11y.cy.ts**. Add a test that opens the landing page (just `cy.visit('');`) and then execute the following commands:
  - `cy.injectAxe()`
  - `cy.checkA11y()`
- You should see that this test fails. 4 violations are shown. Click on them and study the explanation in the console.

# 5. Failing Test I: _should rename Angelika Hoffmann to Angelika Hofmann_

In **/cypress/e2e/failing.cy.ts**, the _should rename Angelika Hoffmann to Angelika Hofmann_ fails for some reason. Find out why and fix it.

<details>
<summary>Show Solution</summary>
<p>

```typescript
it("should rename Angelika Hoffmann to Angelika Hofmann", () => {
  cy.testid("btn-customers").click();
  cy.contains("[data-testid=row-customer]", "Angelika Hoffmann")
    .find("[data-testid=btn-edit]")
    .click();
  cy.testid("inp-name").should("have.value", "Hoffmann").clear();
  cy.testid("inp-name").type("Hofmann");
  cy.testid("btn-submit").click();

  cy.testid("row-customer").should("contain.text", "Angelika Hofmann");
});
```

</p>
</details>

# 6. Buggy Test II: _should remove Knut Eggen_

The test _should remove Knut Eggen_ seems to work, but that's not true. The assertion is wrong. Find out why, and fix it.

Hint: You might want force the test to fail.

<details>
<summary>Show Solution</summary>
<p>

```typescript
it("should remove Knut Eggen", () => {
  cy.testid("btn-customers").click();
  cy.contains("[data-testid=row-customer]", "Knut Eggen")
    .find("[data-testid=btn-edit]")
    .click();
  cy.testid("btn-delete").click();
  cy.testid("row-customer").should("not.contain.text", "Knut Eggen");
});
```

</p>
</details>

# 7. Buggy Test III: _should toggle the loading indicator_

Also this test doesn't work as expected. Fix it.

Hint: The assertion is wrong and a second assertion is missing.

<details>
<summary>Show Solution</summary>
<p>

```typescript
it("should toggle the loading indicator", () => {
  cy.testid("btn-holidays").click();
  cy.testid("loading-indicator").should("be.visible");
  cy.testid("loading-indicator").should("not.exist");
});
```

</p>
</details>

# 8. Scraping with `cy.task`

Write a test that reads all names of the customer's first page and stores them to a file called **customers.txt**.
