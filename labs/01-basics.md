- [1. Setup](#1-setup)
- [2. Add a first sanity check test](#2-add-a-first-sanity-check-test)
- [3. Assertions I](#3-assertions-i)
- [4. Assertion II](#4-assertion-ii)
- [5. Count the listed customers](#5-count-the-listed-customers)
- [6. Edit a customer's firstname](#6-edit-a-customers-firstname)
- [7. Add a new customer](#7-add-a-new-customer)
- [8. Request Brochure for Firenze](#8-request-brochure-for-firenze)
- [9. `cy.testid`](#9-cytestid)
- [10. Split up the tests](#10-split-up-the-tests)
- [11. Run/CI in Mode](#11-runci-in-mode)
- [12. Webkit](#12-webkit)

To solve the exercises in this lab, consult the slides and the [official documentation](https://docs.cypress.io/guides/end-to-end-testing/writing-your-first-end-to-end-test).

Try to use the attribute `data-testid` as the criteria for your selectors.

The solution branch is _cy-01-solution_

# 1. Setup

1. Open the **/cypress/e2e** folder and make yourself acquainted with its content.

# 2. Add a first sanity check test

Create a new **/cypress/e2e/init.cy.ts** file and add the following first test:

<details>
<summary>Show Solution</summary>
<p>

```typescript
describe("init", () => {
  beforeEach(() => {
    cy.visit("");
  });

  it("should do a sanity check", () => {});
});
```

</p>
</details>

# 3. Assertions I

Assert that the side-menu has a link which contains the text Holidays. Use the `data-testid` attribute as selector.

<details>
<summary>Show Solution</summary>
<p>

**/cypress/e2e/init.cy.ts**

```typescript
it("should do an implicit subject assertion", () => {
  cy.get("[data-testid=btn-holidays]").should("have.text", "Holidays");
});
```

</p>
</details>

# 4. Assertion II

Add two tests where the first does an implicit and the other explicit assertions against the holidays link in the sidemenu:

1. It should have the text "Holidays".
2. It should have the CSS class "mat-mdc-raised-button".
3. It should have an attribute "href" with value "/holidays".
4. Its text should have the colour "rgb(0, 0, 0)".

<details>
<summary>Show Solution</summary>
<p>

**/cypress/e2e/init.cy.ts**

```typescript
it("should verify the holidays link with implicit assertions", () => {
  cy.get("[data-testid=btn-holidays]")
    .should("have.text", "Holidays")
    .and("have.class", "mat-mdc-raised-button")
    .and("have.attr", "href", "/holidays")
    .and("have.css", "color", "rgb(0, 0, 0)");
});

it("should verify the holidays link with explicit assertions", () => {
  cy.get("[data-testid=btn-holidays]").should(($button) => {
    expect($button).to.have.text("Holidays");
    expect($button).to.have.class("mat-mdc-raised-button");
    expect($button).to.have.attr("href", "/holidays");
    expect($button).to.have.css("color", "rgb(0, 0, 0)");
  });
});
```

</p>
</details>

# 5. Count the listed customers

Go to the customers list assert the amount of the rows. It should be exactly ten.

<details>
<summary>Show Solution</summary>
<p>

**/cypress/e2e/init.cy.ts**

```typescript
it("should count the entries", () => {
  cy.get("[data-testid=btn-customers]").click();
  cy.get("[data-testid=row-customer]").should("have.length", 10);
});
```

</p>
</details>

# 6. Edit a customer's firstname

Rename Latitia to Laetitia via the form. Make sure to start typing into the firstname's input field, once its value has been set.

<details>
<summary>Show Solution</summary>
<p>

**/cypress/e2e/init.cy.ts**

```typescript
it("should rename Latitia to Laetitia", () => {
  cy.get("[data-testid=btn-customers]").click();
  cy.contains("[data-testid=row-customer]", "Latitia")
    .find("[data-testid=btn-edit]")
    .click();
  cy.get("[data-testid=inp-firstname]").clear().type("Laetitia");
  cy.get("[data-testid=btn-submit]").click();

  cy.get("[data-testid=row-customer]").should(
    "contain.text",
    "Laetitia Bellitissa"
  );
});
```

</p>
</details>

# 7. Add a new customer

Add a new customer and check if it appears on the listing page.

<details>
<summary>Show Solution</summary>
<p>

**/cypress/e2e/init.cy.ts**

```typescript
it("should add a new customer", () => {
  cy.get("[data-testid=btn-customers]").click();
  cy.get("[data-testid=btn-customers-add]").click();
  cy.get("[data-testid=inp-firstname]").type("Tom");
  cy.get("[data-testid=inp-name]").type("Lincoln");
  cy.get("[data-testid=sel-country]").click();
  cy.get("[data-testid=opt-country]").contains("USA").click();
  cy.get("[data-testid=inp-birthdate]").type("12.10.1995");
  cy.get("[data-testid=btn-submit]").click();
  cy.get("[data-testid=btn-customers-next]").click();

  cy.get("[data-testid=row-customer]").should("contain.text", "Tom Lincoln");
});
```

</p>
</details>

# 8. Request Brochure for Firenze

Write a test that clicks on the Florence holiday's "Get a Brochure" button and verifies that the input "Domgasse 5" shows "Brochure sent".

<details>
<summary>Show Solution</summary>
<p>

**/cypress/e2e/init.cy.ts**

```typescript
it("should request brochure for Firenze", () => {
  cy.get("[data-testid=btn-holidays]").click();
  cy.contains("[data-testid=holiday-card]", "Firenze")
    .find("[data-testid=btn-brochure]")
    .click();
  cy.get("[data-testid=ri-address]").type("Domgasse 5");
  cy.get("[data-testid=ri-search]").click();
  cy.get("[data-testid=ri-message]").should("contain.text", "Brochure sent");
});
```

</p>
</details>

# 9. `cy.testid`

Since we decided to use the `data-testid` all the time, it is better to come up with a shortcut.

Create the command that allows you to write `cy.test('btn-holidays')` instead of `cy.get('[data-testid=btn-holidays'])`.

Make use of the command in your existing tests.

<details>
<summary>Show Solution</summary>
<p>

```typescript

```

</p>
</details>

# 10. Split up the tests

Split up the test into 3 different spec files:

- `customers.cy.ts`
- `holidays.cy.ts`
- `misc.cy.ts`

# 11. Run/CI in Mode

Run the tests via `npm run e2e`. You should see no browser and an output via the console. All tests should end successfully and there should be a video folder, where you can see the recordings of the run.

For the CI mode, enable one retry globally. For the test **should do an implicit subject assertion**, it should have a retry of two.

**Hint**: You can override the global test configuration, by passing a second parameter to the `it` function: `it(name: string, configOverride: {}, testFn: () => void)`.

Make your tests fail and verify that Cypress retries them.

# 12. Webkit

Try if your tests can also run with the experimental webkit browser.

Check out the **cypress.config.ts** and make sure that it has the value `experimentalWebKitSupport` set to true.
