- [1. Page Object Model: Customer Form \& Sidemenu](#1-page-object-model-customer-form--sidemenu)
- [2. `cy.origin`](#2-cyorigin)
- [3. `cy.session`](#3-cysession)
- [4. `cy.login`](#4-cylogin)
- [5. Configurable Credentials](#5-configurable-credentials)
- [6. Reading from the environment](#6-reading-from-the-environment)
- [7. 6 Profiles](#7-6-profiles)
- [8. Re-use and invalidate session](#8-re-use-and-invalidate-session)

Checkout the branch **cy-04-advanced-ii-starter**.

# 1. Page Object Model: Customer Form & Sidemenu

The `should add a new customer` should be runnable by following code:

```typescript
it("should add a new customer", () => {
  sidemenu.open("Customers");
  cy.findByRole("link", { name: "Add Customer" }).click();
  customer.setFirstname("Tom");
  customer.setName("Lincoln");
  customer.setCountry("USA");
  customer.setBirthday(new Date(1995, 9, 12));
  customer.submit();
  cy.testid("btn-customers-next").click();

  cy.testid("row-customer").should("contain.text", "Tom Lincoln");
});
```

Implement the required page object `customer` and `sidemenu`.

<details>
<summary>Show Solution</summary>
<p>

**cypress/page-objects/sidemenu.ts**

```typescript
class Sidemenu {
  open(name: "Customers" | "Holidays") {
    cy.findByRole("link", { name }).click();
  }
}

export const sidemenu = new Sidemenu();
```

**cypress/page-objects/customer.ts**

```typescript
import { format } from "date-fns";

class Customer {
  setFirstname(firstname: string) {
    cy.findByLabelText("Firstname").clear().type(firstname);
  }

  setName(name: string) {
    cy.findByLabelText("Name").clear().type(name);
  }

  setCountry(country: string) {
    cy.findByLabelText("Country").click();
    cy.findByRole("option", { name: country }).click();
  }

  setBirthday(date: Date) {
    cy.findByLabelText("Birthdate").clear().type(format(date, "dd.MM.yyyy"));
  }

  submit() {
    cy.findByRole("button", { name: "Save" }).click();
  }
}

export const customer = new Customer();
```

</p>
</details>

# 2. `cy.origin`

It is quite common to use a signed-in user for the majority of the tests.

Our application uses Auth0 as authentication provides. In a first step, we'll do a login where we switch directly to Auth0 and enter the credentials there.

In a further step, we will cache the session data by using `cy.session`.

Third, we'll pack the session cache along the login into an own command.

Create a new file **session.cy.ts**.

Let's start with a simple login on a different domain. We have to use `cy.origin` in all places, where we leave our origin domain which is specified in the **cypress.config.ts**.

**cypress/e2e/session.cy.ts**

```typescript
describe("Session", () => {
  it("should login", () => {
    cy.visit("");
    cy.findByRole("button", { name: "Sign In" }).click();
    cy.origin("dev-xbu2-fid.eu.auth0.com/", () => {
      cy.get(".auth0-lock-input-email").type("john.list@host.com");
      cy.get(".auth0-lock-input-show-password").type("John List");
      cy.get(".auth0-lock-submit").click();
    });
    cy.testid("p-username").should("have.text", "Welcome John List");
  });
  cy.visit("");
});
```

# 3. `cy.session`

Let's wrap the whole login procedure into a `cy.session` so that it gets cached internally:

**cypress/e2e/session.cy.ts**

```typescript
it("should login", () => {
  cy.session("john-list", () => {
    cy.visit("");

    // rest of the test
  });
});
```

1. Run the test once. Make sure it succeeds and note the execution time.
2. Now rerun. It should be much shorter (well below 1 second).
3. Click on "Clear All Sessions", and rerun the test. You should see that it is not using the cache.

# 4. `cy.login`

Create a new Cypress command, `cy.login` that takes a username and password as parameter. Move the code for the session into it. Make sure the parameters for username and password are used as cache key (first parameter of `cy.session`).

**Note**: If you want to reuse variables inside a `cy.origin`, you have to pass them in an object literal `{args: any}` as second paramter of `cy.origin`. The actual function gets them as first parameter.

Add a test that doesn't use the session. Assert that the second test has not a signed in user.

<details>
<summary>Show Solution</summary>
<p>

**cypress/support/commands.ts**

```typescript
// command declaration...

// other commands...

Cypress.Commands.add("login", (username: string, password: string) => {
  cy.session({ username, password }, () => {
    cy.visit("");
    cy.findByRole("button", { name: "Sign In" }).click();
    cy.origin("dev-xbu2-fid.eu.auth0.com/", { args: { username, password } }, ({ username, password }) => {
      cy.get(".auth0-lock-input-email").type(username);
      cy.get(".auth0-lock-input-show-password").type(password);
      cy.get(".auth0-lock-submit").click();
    });
    cy.testid("p-username").should("have.text", "Welcome John List");
  });
});
```

**cypress/e2e/session.cy.ts**

```typescript
describe("Session", () => {
  it("should reuse the session", () => {
    cy.login("john.list@host.com", "John List");
    cy.visit("");
    cy.testid("p-username").should("have.text", "Welcome John List");
  });

  it("should not reuse the session", () => {
    cy.visit("");
    cy.testid("p-username").should("not.exist");
  });
});
```

</p>
</details>

# 5. Configurable Credentials

Create with `loginUsername` and `loginPassword` two environment variables in the `cypress.config.ts`.

Update `cy.login`, so that it is callable without parameters and uses the one from the environment as default values.

<details>
<summary>Show Solution</summary>
<p>

**cypress.config.ts**

```typescript
import { defineConfig } from "cypress";
import { writeFile } from "fs/promises";

export default defineConfig({
  e2e: {
    // ...

    env: {
      loginUsername: "john.list@host.com",
      loginPassword: "John List",
    },
  },

  // ...
});
```

**cypress/support/commands.ts**

```typescript
Cypress.Commands.add("login", (username?: string, password?: string) => {
  cy.session({ username, password }, () => {
    cy.visit("");
    cy.findByRole("button", { name: "Sign In" }).click();
    cy.origin(
      "dev-xbu2-fid.eu.auth0.com/",
      {
        args: {
          username: username || Cypress.env("loginUsername"),
          password: password || Cypress.env("loginPassword"),
        },
      },
      ({ username, password }) => {
        cy.get(".auth0-lock-input-email").type(username);
        cy.get(".auth0-lock-input-show-password").type(password);
        cy.get(".auth0-lock-submit").click();
      },
    );
    cy.testid("p-username").should("have.text", "Welcome John List");
  });
});
```

</p>
</details>

# 6. Reading from the environment

Override the Cypress environment variables with those from the operating system. Use the following values:

- username: anna.brecht@host.com
- password: Anna Brecht

(The test will ultimately fail, because your assertions is against "Welcome John List").

# 7. 6 Profiles

Upgrade the login command so that it doesn't expect the username or password anymore. Instead, the caller can choose between a "Standard" and an "Admin" profile. The profile parameter should be union type. That is: `'Standard'| 'Admin'`. The default value is 'Standard'.

<details>
<summary>Show Solution</summary>
<p>

**cypress.config.ts**

```typescript
export default defineConfig({
  e2e: {
    // ...

    env: {
      loginUsernameStandard: "john.list@host.com",
      loginPasswordStandard: "John List",
      loginUsernameAdmin: "anna.brecht@host.com",
      loginPasswordAdmin: "Anna Brecht",
    },

    // ...
  },

  // ...
});
```

**cypress/support/commands.ts**

```typescript
declare namespace Cypress {
  interface Chainable<Subject> {
    // ...
    login(profile?: "Standard" | "Admin"): void;
  }
}

Cypress.Commands.add("login", (profile: "Standard" | "Admin" = "Standard") => {
  const username = Cypress.env(`loginUsername${profile}`);
  const password = Cypress.env(`loginPassword${profile}`);

  cy.session({ username, password }, () => {
    cy.visit("");
    cy.findByRole("button", { name: "Sign In" }).click();
    cy.origin(
      "dev-xbu2-fid.eu.auth0.com/",
      {
        args: {
          username,
          password,
        },
      },
      ({ username, password }) => {
        cy.get(".auth0-lock-input-email").type(username);
        cy.get(".auth0-lock-input-show-password").type(password);
        cy.get(".auth0-lock-submit").click();
      },
    );
    cy.testid("p-username").should("have.text", "Welcome John List");
  });
  cy.visit("");
});
```

</p>
</details>

# 8. Re-use and invalidate session

The cache only works for a single test file.

Rename the current file to **session-1.cy.ts**. Create another with the filename **session-2.cy.ts**.

Add the following code:

```typescript
describe("Session 2", () => {
  it("should reuse the session", () => {
    cy.login();
  });
});
```

Now run only the two tests via `npx cypress run --spec '**/session-1.cy.ts,**/session-2.cy.ts'`. Mind the `'`.

You should see that the second one lasts almost as long as the first one. Hence it re-creates the session.

Lookup the signature of the `cy.session` and find out, if there is an option to change that.

You want to set a conditional value

<details>
<summary>Show Solution</summary>
<p>

The `cy.session` command has a third parameter, where we can pass options.

```typescript
cy.session(
  "[someid]",
  () => {
    // session code
  },
  { cacheAcrossSpecs: true },
);
```

</p>
</details>
